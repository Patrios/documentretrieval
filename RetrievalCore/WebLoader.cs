using System.Net;

namespace RetrievalCore
{
	public static class WebLoader
	{
		public static void LoadPage(string pageUri, string localPath)
		{
			WebClient.DownloadFile (pageUri, localPath);
		}

		private static readonly WebClient WebClient = new WebClient(); 
	}
}

