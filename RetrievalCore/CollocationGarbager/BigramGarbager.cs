﻿using System.Collections.Generic;
using RetrievalCore.TextSpliter;

namespace RetrievalCore.CollocationGarbager
{
    public class BigramGarbager : IBigramGarbager
    {
        public BigramGarbager(
            ITextSpliter proposalTextSpliter, 
            ITextSpliter wordSpliter, 
            int windowSize)
        {
            _proposalTextSpliter = proposalTextSpliter;
            _wordSpliter = wordSpliter;
            _windowSize = windowSize;

            if (_windowSize < MinSize)
            {
                _windowSize = MinSize;
            }
        }

        public BigramGarbageResult Collect(string text)
        {
            var result = new BigramGarbageResult();

            foreach (var proposal in _proposalTextSpliter.Split(text))
            {
                CollectProposal(proposal, result);
            }

            return result;
        }

        private void CollectProposal(string proposal, BigramGarbageResult garbageResult)
        {
            var words = _wordSpliter.Split(proposal);

            if (words.Length > 2)
            {
                CollectCollocations(words, garbageResult);
            }
        }

        private void CollectCollocations(IList<string> words, BigramGarbageResult garbageResult)
        {
            var startWord = 0;
            var finishWord = startWord + MinSize - 1;
            do
            {
                for (var i = startWord; i < finishWord; i++)
                {
                    garbageResult.AppendBigram(new Bigram(words[i], words[finishWord]), finishWord - i);
                }

                finishWord++;
                if (finishWord >= _windowSize)
                {
                    startWord++;
                }
            } while (finishWord != words.Count);
        }

        private readonly int _windowSize;
        private readonly ITextSpliter _proposalTextSpliter;
        private readonly ITextSpliter _wordSpliter;

        private const int MinSize = 2;
    }
}
