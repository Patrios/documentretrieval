﻿using System.Collections.Generic;

namespace RetrievalCore.CollocationGarbager
{
    public sealed class BigramComparer : IEqualityComparer<Bigram>
    {
        public bool Equals(Bigram x, Bigram y)
        {
            return x.RightWord == y.RightWord && x.LeftWord == y.LeftWord;
        }

        public int GetHashCode(Bigram obj)
        {
            return obj.GetHashCode();
        }
    }
}
