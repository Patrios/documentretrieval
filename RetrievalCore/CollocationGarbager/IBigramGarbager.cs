﻿namespace RetrievalCore.CollocationGarbager
{
    public interface IBigramGarbager
    {
        BigramGarbageResult Collect(string text);
    }
}
