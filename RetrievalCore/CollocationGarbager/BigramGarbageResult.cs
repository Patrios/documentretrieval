﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore.CollocationGarbager
{
    public sealed class BigramGarbageResult
    {
        public BigramGarbageResult()
        {
            CountPerWords = new Dictionary<string, int>();
            BigramsStatistics = new Dictionary<Bigram, BigramStatistics>(_comparer);
        }

        public void AppendBigram(Bigram bigram, int distance)
        {
            if (BigramsStatistics.ContainsKey(bigram))
            {
                BigramsStatistics[bigram].Distances.Add(distance);
            }
            else
            {
                BigramsStatistics.Add(bigram, new BigramStatistics(distance));
            }

            AppendWordInBigram(bigram.LeftWord);
            AppendWordInBigram(bigram.RightWord);
            BigramsCount++;
        }

        public void CalculateStatstic()
        {
            foreach (var bigram in BigramsStatistics)
            {
                CalculateAvg(bigram);
                CalculateDistanveVariance(bigram);
                CalculateStudentTest(bigram);
                CalculatePirsonTest(bigram);
              //  CalculateLikelihood(bigram);
            }
        }

        public int BigramsCount { get; private set; }

        public IDictionary<Bigram, BigramStatistics> BigramsStatistics { get; private set; }

        private void AppendWordInBigram(string word)
        {
            if (CountPerWords.ContainsKey(word))
            {
                CountPerWords[word]++;
            }
            else
            {
                CountPerWords.Add(word, 1);
            }
        }

        private void CalculateLikelihood(KeyValuePair<Bigram, BigramStatistics> bigram)
        {
            var leftWordCount = WordContainBigramCount(bigram.Key.LeftWord);
            var rightWordCount = WordContainBigramCount(bigram.Key.RightWord);
            var bigramCount = bigram.Value.Count;

            var p = (double) rightWordCount/BigramsCount;
            var p1 = (double) bigramCount/leftWordCount;
            var p2 = (double) (rightWordCount - bigramCount) / (BigramsCount - leftWordCount);

            var l1 = Math.Log(BernoulliScheme(bigramCount, leftWordCount, p));
            var l2 = Math.Log(BernoulliScheme(rightWordCount - bigramCount, BigramsCount - leftWordCount, p));
            var l3 = Math.Log(BernoulliScheme(bigramCount, leftWordCount, p1));
            var l4 = Math.Log(BernoulliScheme(rightWordCount - bigramCount, BigramsCount - leftWordCount, p2));

            bigram.Value.LikelihoodTest = l1 + l2 - l3 - l4;
        }

        private double BernoulliScheme(int k, int n, double x)
        {
            return Math.Pow(x, k)*Math.Pow(1 - x, n - k);
        }

        private void CalculatePirsonTest(KeyValuePair<Bigram, BigramStatistics> bigram)
        {
            var bigramCount = bigram.Value.Count;
            var leftOnlyWordCount = WordOnlyContainBigramCount(bigram.Key.LeftWord, bigram.Value);
            var rightOnlyWordCount = WordOnlyContainBigramCount(bigram.Key.RightWord, bigram.Value);
            var allOtherBigramCount = BigramsCount - bigramCount;

            var devider = (double)(bigramCount + rightOnlyWordCount) *
                          (bigramCount + leftOnlyWordCount)*
                          (rightOnlyWordCount + allOtherBigramCount)*
                          (leftOnlyWordCount + allOtherBigramCount);

            var deviden = (double)BigramsCount *
                          (bigramCount*allOtherBigramCount - rightOnlyWordCount*leftOnlyWordCount)*
                          (bigramCount*allOtherBigramCount - rightOnlyWordCount*leftOnlyWordCount);

            bigram.Value.PirsonTest = deviden / devider;
        }

        private void CalculateStudentTest(KeyValuePair<Bigram, BigramStatistics> bigram)
        {
            var leftWordCount = WordContainBigramCount(bigram.Key.LeftWord);
            var rightWordCount = WordContainBigramCount(bigram.Key.RightWord);

            var leftWordChance = leftWordCount / (double)BigramsCount;
            var rightWordChance = rightWordCount / (double)BigramsCount;

            var choosAvg = bigram.Value.Count / (double)BigramsCount;
            var expectation = leftWordChance*rightWordChance;
            var variance = choosAvg;

            var devider = Math.Sqrt((variance * variance) / BigramsCount);
            var delta = choosAvg - expectation;

            bigram.Value.StudentTest = delta / devider;
        }

        private int WordOnlyContainBigramCount(string word, BigramStatistics containBigram)
        {
            return WordContainBigramCount(word) - containBigram.Count;
        }

        private int WordContainBigramCount(string word)
        {
            return CountPerWords[word];
        }

        private void CalculateAvg(KeyValuePair<Bigram, BigramStatistics> bigram)
        {
            bigram.Value.AvgDistance = bigram.Value.Distances.Sum(x => x) / (double)bigram.Value.Count;
        }

        private void CalculateDistanveVariance(KeyValuePair<Bigram, BigramStatistics> bigram)
        {
            bigram.Value.DistanceVariance = bigram.Value.Distances.Sum(
                x => (x - bigram.Value.AvgDistance) * (x - bigram.Value.AvgDistance))
                                            / (bigram.Value.Count - 1);
        }

        private IDictionary<string, int> CountPerWords { get; set; }
        private readonly BigramComparer _comparer = new BigramComparer();
    }
}
