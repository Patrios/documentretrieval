﻿using System;

namespace RetrievalCore.CollocationGarbager
{
    public sealed class Bigram : IEquatable<Bigram>
    {
        public Bigram(string leftWord, string rightWord)
        {
            LeftWord = leftWord;
            RightWord = rightWord;
        }

        public string LeftWord { get; private set; }
       
        public string RightWord { get; private set; }

        public override int GetHashCode()
        {
            return LeftWord.GetHashCode() | RightWord.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Bigram);
        }

        public bool Equals(Bigram obj)
        {
            return (LeftWord == obj.LeftWord && RightWord == obj.RightWord);
        }
    }
}
