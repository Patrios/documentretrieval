﻿using System.Collections.Generic;

namespace RetrievalCore.CollocationGarbager
{
    public class BigramStatistics
    {
        public BigramStatistics(int distance)
        {
            Distances = new List<int>() { distance };
        }

        public int Count 
        {
            get { return Distances.Count; }
        }

        public IList<int> Distances { get; private set; } 

        public double AvgDistance { get; set; }

        public double DistanceVariance { get; set; }

        public double StudentTest { get; set; }

        public double PirsonTest { get; set; }

        public double LikelihoodTest { get; set; }
    }
}
