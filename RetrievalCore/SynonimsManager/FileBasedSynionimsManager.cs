﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using RetrievalCore.TermsGarbegers;
using RetrievalCore.TermsGarbegers.TokensBreaker;

namespace RetrievalCore.SynonimsManager
{
    public class FileBasedSynionimsManager : ISynonimsManager
    {
        public FileBasedSynionimsManager(IDictionary<string, List<string>> synonims)
        {
            _synonims = synonims;
        }

        public IEnumerable<string> GetSynonims(string word)
        {
            return _synonims.ContainsKey(word) ? _synonims[word] : Enumerable.Empty<string>();
        }

        private readonly IDictionary<string, List<string>> _synonims;
    }
}
