﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using RetrievalCore.TermsGarbegers.TokensBreaker;

namespace RetrievalCore.SynonimsManager
{
    public sealed class SynonimsParser
    {
        public SynonimsParser(IWordBreaker wordBreaker)
        {
            _wordBreaker = wordBreaker;
        }

        public IDictionary<string, List<string>> Parse(string synonimsFile)
        {
            var result = new Dictionary<string, List<string>>();
            using (var streamReader = new StreamReader(synonimsFile))
            {
                while (!streamReader.EndOfStream)
                {
                    // ReSharper disable PossibleNullReferenceException
                    var synonimLine = streamReader.ReadLine().Split(SynonimSeparator);
                    // ReSharper restore PossibleNullReferenceException

                    if (synonimLine.Length != 2)
                    {
                        continue;
                    }

                    var synonimLeft = _wordBreaker.BreakByWords(synonimLine.First()).ToArray();

                    if (synonimLeft.Length != 1)
                    {
                        continue;
                    }

                    var synonimWord = synonimLeft.First();

                    var synonims = synonimLine.Last().Split(InWordsSeparator);
                    var resultSynonims = new List<string>();

                    foreach (var synonim in synonims)
                    {
                        var words = _wordBreaker.BreakByWords(synonim).ToArray();
                        if (words.Length == 1)
                        {
                            resultSynonims.Add(words.First());
                        }
                    }

                    if (resultSynonims.Count == 0)
                    {
                        continue;
                    }

                    if (result.ContainsKey(synonimWord))
                    {
                        result[synonimWord].AddRange(resultSynonims);
                    }
                    else
                    {
                        result.Add(synonimWord, resultSynonims);
                    }
                }
            }

            return result;
        }

        private readonly IWordBreaker _wordBreaker;

        private const char SynonimSeparator = '|';
        private const char InWordsSeparator = ',';
    }
}
