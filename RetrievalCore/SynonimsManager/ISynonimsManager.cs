﻿using System.Collections.Generic;

namespace RetrievalCore.SynonimsManager
{
    public interface ISynonimsManager
    {
        IEnumerable<string> GetSynonims(string word);
    }
}
