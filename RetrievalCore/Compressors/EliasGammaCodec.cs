﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore.Compressors
{
    [Serializable]
    public class EliasGammaCodec : ResidualCodec
    {
        protected override ICollection<byte> EncodeResidualData(ICollection<int> data)
        {
            var resultBools = new List<bool>();

            foreach (var number in data)
            {
                var oprNumber = number + 1;
                var log = (int)Math.Log(oprNumber, 2);

                for (var i = 0; i < log; i++)
                {
                    resultBools.Add(false);
                }
                resultBools.Add(true);

                for (var i = 0; i < log; i++)
                {
                    if ((oprNumber & (1 << i)) != 0)
                    {
                        resultBools.Add(true);
                    }
                    else
                    {
                        resultBools.Add(false);
                    }
                }
            }

            var bitArray = new BitArray(resultBools.ToArray());

            var byteArray = new byte[bitArray.Length / 8 + ((bitArray.Length % 8) == 0 ? 0 : 1) + 1];
            bitArray.CopyTo(byteArray, 0);
            
            int offset = byteArray.Length*8 - bitArray.Length;

            byteArray[byteArray.Length - 1] = (byte) offset;

            return byteArray;
        }

        protected override ICollection<int> DecodeResidualData(ICollection<byte> data)
        {
            var numbers = new List<int>();

            var bitArray = new BitArray(data.ToArray());

            int offset = data.Last();

            int zeroBits;
            var upper = bitArray.Length - offset;
            for (var i = 0; i < upper;)
            {
                zeroBits = 0;
                while (!bitArray[i] || i >= upper)
                {
                    zeroBits++;
                    i++;
                }

                i++;

                int number = 0;
                for (var j = 0; j < zeroBits; j++)
                {
                    if (bitArray[i])
                    {
                        number += 1 << j;
                    }

                    i++;
                }

                number = number | (1 << zeroBits);
                number--;
                numbers.Add(number);
            }

            return numbers;
        }
    }
}
