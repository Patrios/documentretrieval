﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore.Compressors
{
    [Serializable]
    public abstract class ResidualCodec : IDataCodec
    {
        public ICollection<byte> Encode(ICollection<int> data)
        {
            var dataArray = data.ToArray();
            var residualData = new int[data.Count];
            residualData[0] = dataArray[0];

            for (var i = 1; i < data.Count; i++)
            {
                residualData[i] = dataArray[i] - dataArray[i - 1];
            }

            return EncodeResidualData(residualData);
        }

        public ICollection<int> Decode(ICollection<byte> data)
        {
            var residualDataArray = DecodeResidualData(data).ToArray();

            for (var i = 1; i < residualDataArray.Length; i++)
            {
                residualDataArray[i] += residualDataArray[i - 1];
            }

            return residualDataArray;
        }

        protected abstract ICollection<byte> EncodeResidualData(ICollection<int> data);

        protected abstract ICollection<int> DecodeResidualData(ICollection<byte> data);
    }
}
