﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore.Compressors
{
    [Serializable]
    public sealed class VariableByteCodec : ResidualCodec
    {
        protected override ICollection<byte> EncodeResidualData(ICollection<int> data)
        {
            var bytes = new List<byte>();

            foreach (var number in data)
            {
                bytes.AddRange(EncodeNumber(number));
            }

            return bytes;
        }

        protected override ICollection<int> DecodeResidualData(ICollection<byte> data)
        {
            var dataList = data.ToList();
            var numbers = new List<int>();
            var prevPosition = -1;

            for (var i = 0; i < data.Count; i++)
            {
                if (dataList[i] >= MaxByte)
                {
                    var number = dataList[i] - MaxByte;
                    for (var j = i - 1; j > prevPosition; j--)
                    {
                        number = MaxByte * number + dataList[j];
                    }

                    prevPosition = i;
                    numbers.Add(number);
                }
            }

            return numbers;
        }

        private static IEnumerable<byte> EncodeNumber(int number)
        {
            var bytes = new List<byte>();

            while (true)
            {
                bytes.Add((byte)(number % MaxByte));

                if (number < MaxByte)
                {
                    break;
                }

                number = number / MaxByte;
            }

            bytes[bytes.Count - 1] += MaxByte;

            return bytes;
        }

        private const int MaxByte = 128;
    }
}
