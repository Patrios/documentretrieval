﻿using System.Collections.Generic;

namespace RetrievalCore.Compressors
{
    public interface IDataCodec
    {
        ICollection<byte> Encode(ICollection<int> data);

        ICollection<int> Decode(ICollection<byte> data);
    }
}
