﻿using System.IO;

namespace RetrievalCore
{
    public class BookDownloader
    {
        public void DownloadKarenina(string corpusFolder)
        {
            var kareninaLocalName = Path.Combine(corpusFolder, KareninaLocalPath);
            if (!File.Exists(kareninaLocalName))
            {
                WebLoader.LoadPage(KareninaPath, kareninaLocalName);
            }
        }

        public void DownloadWarAndPeace1(string corpusFolder)
        {
            var warAndPeacePath = Path.Combine(corpusFolder, WarAndPeace1Path);
            if (!File.Exists(warAndPeacePath))
            {
                WebLoader.LoadPage(WarAndPeace1, warAndPeacePath);
            }
        }

        public void DownloadWarAndPeace2(string corpusFolder)
        {
            var warAndPeacePath = Path.Combine(corpusFolder, WarAndPeace2Path);
            if (!File.Exists(warAndPeacePath))
            {
                WebLoader.LoadPage(WarAndPeace2, warAndPeacePath);
            }
        }

        public void DownloadWarAndPeace3(string corpusFolder)
        {
            var warAndPeacePath = Path.Combine(corpusFolder, WarAndPeace3Path);
            if (!File.Exists(warAndPeacePath))
            {
                WebLoader.LoadPage(WarAndPeace3, warAndPeacePath);
            }
        }

        public void DownloadWarAndPeace4(string corpusFolder)
        {
            var warAndPeacePath = Path.Combine(corpusFolder, WarAndPeace4Path);
            if (!File.Exists(warAndPeacePath))
            {
                WebLoader.LoadPage(WarAndPeace4, warAndPeacePath);
            }
        }

        public string KareninaLocalPath
        {
            get { return "Karenina"; }
        }

        public string WarAndPeace1Path
        {
            get { return "WarAndPeace1"; }
        }

        public string WarAndPeace2Path
        {
            get { return "WarAndPeace2"; }
        }
        
        public string WarAndPeace3Path
        {
            get { return "WarAndPeace3"; }
        }

        public string WarAndPeace4Path
        {
            get { return "WarAndPeace4"; }
        }

        private const string KareninaPath = @"http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0080.shtml";

        private const string WarAndPeace1 = @"http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0040.shtml";

        private const string WarAndPeace2 = @"http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0050.shtml";

        private const string WarAndPeace3 = @"http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0060.shtml";
        
        private const string WarAndPeace4 = @"http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0070.shtml";
    }
}
