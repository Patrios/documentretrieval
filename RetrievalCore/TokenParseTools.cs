﻿using System.Collections.Generic;

namespace RetrievalCore
{
    public static class TokenParseTools
    {
        public static IEnumerable<string> GetScopeSingleTokens(string query)
        {
            var words = query.Split(Consts.Separator);
            foreach (var word in words)
            {
                var token = word.ToLower().Trim();
                var closeNum = 0;

                while (token.StartsWith(Consts.OpenScope) && token.Length > 0)
                {
                    yield return Consts.OpenScope;
                    if (token.Length > 0)
                    {
                        token = token.Substring(1);
                    }
                }

                while (token.EndsWith(Consts.CloseScope) && token.Length > 0)
                {
                    closeNum++;
                    if (token.Length > 0)
                    {
                        token = token.Substring(0, token.Length - 1);
                    }
                }

                if (token.Length > 0)
                {
                    yield return token;
                }

                for (var i = 0; i < closeNum; i++)
                {
                    yield return Consts.CloseScope;
                }
            }
        }
    }
}
