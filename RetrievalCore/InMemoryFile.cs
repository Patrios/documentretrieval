namespace RetrievalCore
{
	public class InMemoryFile
	{
		public InMemoryFile(string content)
		{
			Id = _lastId;
		    _lastId++;

			Content = content;
		}

		public int Id { get; private set; }
		public string Content { get; private set; }

	    private static int _lastId;
	}
}

