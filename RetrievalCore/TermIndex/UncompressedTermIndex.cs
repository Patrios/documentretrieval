using System;
using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore.TermIndex
{
    [Serializable]
	public class UncompressedTermIndex : ITermIndex
	{
        private UncompressedTermIndex()
        {
            _positions = new Dictionary<int, IList<int>>();
        }

        public UncompressedTermIndex(IDictionary<int, IList<int>> positions)
        {
            _positions = positions;
        }

		public int TotalCount 
        {
		    get { return _positions.Sum(x => x.Value.Count); }
        }

        public IDictionary<int, IList<int>> GetAllPositions()
        {
            return _positions;
        }

        public IList<int> GetPositionsInFile(int fileId)
        {
            return _positions[fileId];
        }

        public void AddNewPosition(int fileId, int position)
        {
            if (!_positions.ContainsKey(fileId))
            {
                _positions[fileId] = new List<int>();
            }

            _positions[fileId].Add(position);
        }

        private readonly IDictionary<int, IList<int>> _positions;
	}
}

