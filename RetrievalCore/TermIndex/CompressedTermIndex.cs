﻿using System;
using System.Collections.Generic;
using System.Linq;
using RetrievalCore.Compressors;

namespace RetrievalCore.TermIndex
{
    [Serializable]
    public class CompressedTermIndex : ITermIndex
    {
        private CompressedTermIndex()
        {
            TotalCount = 0;
        }

        public CompressedTermIndex(
            int totalCount, 
            IDataCodec codec,
            IDictionary<int, ICollection<byte>> codedIndex)
        {
            _codec = codec;
            _codedIndex = codedIndex;
            TotalCount = totalCount;
        }

        public int TotalCount { get; private set; }

        public IDictionary<int, IList<int>> GetAllPositions()
        {
            var decodedData = new Dictionary<int, IList<int>>();

            foreach (var index in _codedIndex)
            {
                decodedData.Add(index.Key, _codec.Decode(index.Value).ToList());
            }

            return decodedData;
        }

        public IList<int> GetPositionsInFile(int fileId)
        {
            return _codec.Decode(_codedIndex[fileId]).ToList();
        }

        public void AddNewPosition(int fileId, int position)
        {
            throw new System.NotImplementedException();
        }

        private readonly IDataCodec _codec;
        private readonly IDictionary<int, ICollection<byte>> _codedIndex;
    }
}
