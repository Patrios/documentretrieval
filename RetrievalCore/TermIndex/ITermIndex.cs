﻿using System.Collections.Generic;

namespace RetrievalCore.TermIndex
{
    public interface ITermIndex
    {
        int TotalCount { get; }

        IDictionary<int, IList<int>> GetAllPositions();

        IList<int> GetPositionsInFile(int fileId);

        void AddNewPosition(int fileId, int position);
    }
}
