﻿using System.Collections.Generic;
using System.Linq;
using RetrievalCore.Compressors;
using RetrievalCore.TermIndex;
using RetrievalCore.TermsGarbegers;

namespace RetrievalCore
{
    public static class IndexCompressor
    {
        public static Index Compress(Index index, IDataCodec codec)
        {
            var terms = new Dictionary<string, ITermIndex>();

            foreach (var termKeyValue in index.Terms)
            {
                var positions = termKeyValue.Value.GetAllPositions();
                var codedIndex = positions.ToDictionary(
                    posKeyValue => posKeyValue.Key,
                    posKeyValue => codec.Encode(posKeyValue.Value));

                terms.Add(
                    termKeyValue.Key, 
                    new CompressedTermIndex(
                        termKeyValue.Value.TotalCount, 
                        codec, 
                        codedIndex));
            }

            return new Index(terms);
        }
    }
}
