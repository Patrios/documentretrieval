using System.Collections.Generic;
using RetrievalCore.QueryExecutor;

namespace RetrievalCore.SnippetBuilder
{
	public interface ISnippetBuilder
	{
		IEnumerable<string> CollectResults(QueryResult rangedResult);
	}
}

