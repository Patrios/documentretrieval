using System.Collections.Generic;
using RetrievalCore.QueryExecutor;
using System.Linq;
using System.Text;
using RetrievalCore.TermsGarbegers;

namespace RetrievalCore.SnippetBuilder
{
	public class FileAsSnippetBuilder : ISnippetBuilder
	{
		public FileAsSnippetBuilder(IList<InMemoryFile> files, Index statistics)
		{
			_files = files;
            _statistics = statistics;
		}

		public IEnumerable<string> CollectResults(QueryResult rangedResult)
        {
            var builder = new StringBuilder();
			foreach (var fileId in rangedResult.FileIds) 
			{
                builder.Clear();

                var positions = rangedResult.Positions[fileId];
                var wordPositions = _statistics.Terms[Consts.WordKey].GetPositionsInFile(fileId);
			    var file = _files.FirstOrDefault(x => x.Id == fileId);
                if (file != null)
			    {
			        var content = file.Content;

			        foreach (var position in positions)
			        {
			            var startPosition = FindNPrev(wordPositions, Consts.SnippetSize, position) ?? 0;
			            var finishPosition = FindNNext(wordPositions, Consts.SnippetSize, position) ?? content.Length;

			            if (startPosition != 0)
			            {
			                builder.Append("... ");
			            }

			            builder.Append(content.Substring(startPosition, finishPosition - startPosition));

			            if (finishPosition != content.Length)
			            {
			                builder.Append(" ...\n");
			            }
			        }
			    }

			    yield return builder.ToString();
			}
		}

        private static int? FindNPrev(IList<int> positions, int n, int position)
        {
            int? result = position;
            positions = positions.Reverse().ToArray();
            for (var i = 0; i < n; i++)
            {
                result = positions.FirstOrDefault(x => x < result);

                if (result == default(int))
                {
                    return null;
                }
            }

            return result;
        }

        private static int? FindNNext(IList<int> positions, int n, int position)
        {
            n++;
            int? result = position;
            for (var i = 0; i < n; i++)
            {
                result = positions.FirstOrDefault(x => x > result);

                if (result == default(int))
                {
                    return null;
                }
            }

            return result;
        }

	    private readonly IList<InMemoryFile> _files;
        private readonly Index _statistics;
	}
}


