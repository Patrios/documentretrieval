
using System.Collections.Generic;

namespace RetrievalCore.QueryExecutor
{
	public sealed class QueryResult
	{
		public QueryResult(IDictionary<int, IList<int>> filePositions)
		{
			FilePositions = filePositions;
		}

		public IDictionary<int, IList<int>> FilePositions { get; private set; }
	}
}
