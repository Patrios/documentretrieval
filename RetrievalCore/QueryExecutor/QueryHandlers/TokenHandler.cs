using System.Collections.Generic;
using System.Linq;
using RetrievalCore.TermsGarbegers;

namespace RetrievalCore.QueryExecutor.QueryHandlers
{
	public class TokenHandler
	{
		public TokenHandler(Index statistics)
		{
			_statistics = statistics;
		}

		public bool CanHandle(string token)
		{
			return false;
		}

		public QueryResult Handle(string token)
		{
			if (!_statistics.Terms.ContainsKey(token)) 
			{
				return new QueryResult(new int[0], new Dictionary<int, IList<int>>());
			}

            var positions = _statistics.Terms[token].GetAllPositions();
            var fileIds = positions.Keys.ToArray();

		    return new QueryResult(fileIds, positions);
		}

		private readonly Index _statistics;
	}
}

