using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore.QueryExecutor.QueryHandlers
{
	public class OrQueryHandler : IQueryHandler
	{		
		public bool CanHandle(string subQuery)
		{
			return subQuery.Equals (OrPatternKeyword);
		}
		
		public QueryResult Handle(QueryResult left, QueryResult right, string query)
		{
			var resultFiles = new List<int> ();
			resultFiles.AddRange (left.FileIds);
			resultFiles.AddRange (right.FileIds);
            var resultFilesArray = resultFiles.Distinct().OrderBy(x => x).ToArray();
            var positions = left.Positions;

            foreach (var valKey in right.Positions)
            {
                if (positions.ContainsKey(valKey.Key))
                {
                    var newValue = positions[valKey.Key];
                    (newValue as List<int>).AddRange(valKey.Value);
                }
                else
                {
                    positions.Add(valKey);
                }
            }

            return new QueryResult(resultFilesArray, positions);
		}
		
		public int Priority 
		{
			get 
			{
				return 1;
			}
		}

		public static string OrPatternKeyword
		{
			get
			{
				return "or";
			}
		}
	}
}

