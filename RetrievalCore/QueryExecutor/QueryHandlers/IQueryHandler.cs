namespace RetrievalCore.QueryExecutor.QueryHandlers
{
	public interface IQueryHandler
	{
		bool CanHandle(string subQuery);

		QueryResult Handle(QueryResult left, QueryResult right, string query);

		int Priority { get; }
	}
}

