using System;
using RetrievalCore.QueryExecutor;
using RetrievalCore.Analyzers;
using System.Collections.Generic;

namespace RetrievalCore
{
	public class TokenHandler
	{
		public TokenHandler(AnalysisResult statistics)
		{
			_statistics = statistics;
		}

		public bool CanHandle(string token)
		{
			return false;
		}

		public QueryResult Handle(string token)
		{
			IDictionary<int, IList<int>> fileIds = new Dictionary<int, IList<int>> ();
			if (_statistics.Terms.ContainsKey (token)) 
			{
				fileIds = _statistics.Terms[token].FilePositions;
			}

			return new QueryResult(fileIds);
		}

		private AnalysisResult _statistics;
	}
}

