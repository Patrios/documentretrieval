using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore.QueryExecutor.QueryHandlers
{
	public class AndQueryHandler : IQueryHandler
	{		
		public bool CanHandle(string subQuery)
		{
			return subQuery.Equals (_andPatternKeyword);
		}
		
		public QueryResult Handle(QueryResult left, QueryResult right, string query)
		{
			var resultFiles = new List<int> ();
            var positions = new Dictionary<int, IList<int>>();
			int leftIterator = 0, rightIterator = 0;
			int leftId, rightId;

            while (leftIterator < left.FileIds.Length && rightIterator < right.FileIds.Length)
			{
				leftId = left.FileIds[leftIterator];
				rightId = right.FileIds[rightIterator];
				if(leftId == rightId)
				{
                    var filePositions = new List<int> ();
                    filePositions.AddRange(left.Positions[leftId]);
                    filePositions.AddRange(right.Positions[rightId]);

					resultFiles.Add(leftId);
                    positions.Add(leftId, filePositions.Distinct().OrderBy(x => x).ToList());

					leftIterator++;
					rightIterator++;
				}
				else if(leftId < rightId)
				{
					leftIterator++;
				}
				else if(rightId < leftId)
				{
					rightIterator++;
				}

			} 

			return new QueryResult(resultFiles.ToArray(), positions);
		}

		public int Priority 
		{
			get 
			{
				return 2;
			}
		}

		private string _andPatternKeyword = "and";
	}
}
