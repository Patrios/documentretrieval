using RetrievalCore.QueryExecutor;
using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore
{
	public class OrQueryHandler : IQueryHandler
	{		
		public bool CanHandle(string subQuery)
		{
			return subQuery.Equals (OrPatternKeyword);
		}
		
		public QueryResult Handle(QueryResult left, QueryResult right, string query)
		{
			var resultFiles = left.FilePositions;

			foreach (var keyValue in right.FilePositions) {
				if(resultFiles.ContainsKey(keyValue.Key))
				{
					var res = resultFiles[keyValue.Key];
					foreach(var value in keyValue.Value)
					{
						res.Add(value);
					}

					resultFiles[keyValue.Key] = res.Distinct().OrderBy(x => x).ToArray();
				}
			}

			return new QueryResult(resultFiles);
		}
		
		public int Priority 
		{
			get 
			{
				return 1;
			}
		}

		public static string OrPatternKeyword
		{
			get
			{
				return "or";
			}
		}
	}
}

