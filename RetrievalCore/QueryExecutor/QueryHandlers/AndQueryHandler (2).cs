using RetrievalCore.QueryExecutor;
using System.Collections.Generic;
using System.Linq;

namespace RetrievalCore
{
	public class AndQueryHandler : IQueryHandler
	{		
		public bool CanHandle(string subQuery)
		{
			return subQuery.Equals (_andPatternKeyword);
		}
		
		public QueryResult Handle(QueryResult left, QueryResult right, string query)
		{
			var resultFiles = new Dictionary<int, IList<int>> ();
			int leftIterator = 0, rightIterator = 0;
			int leftId, rightId;
			var leftKeys = left.FilePositions.Keys.ToArray ();
			var rightKeys = right.FilePositions.Keys.ToArray ();

			do 
			{
				leftId = leftKeys[leftIterator];
				rightId = rightKeys[rightIterator];
				if(leftId == rightId)
				{
					var position = new List<int>();
					position.AddRange(left.FilePositions[leftId]);
					position.AddRange(right.FilePositions[rightId]);

					resultFiles.Add(leftId, position.Distinct().OrderBy(x => x).ToList());
					leftIterator++;
					rightIterator++;
				}
				else if(leftId < rightId)
				{
					leftIterator++;
				}
				else if(rightId < leftId)
				{
					rightIterator++;
				}

			} while(leftIterator < left.FilePositions.Keys.Count && rightIterator < right.FilePositions.Keys.Count);

			return new QueryResult(resultFiles);
		}

		public int Priority 
		{
			get 
			{
				return 2;
			}
		}

		private string _andPatternKeyword = "and";
	}
}
