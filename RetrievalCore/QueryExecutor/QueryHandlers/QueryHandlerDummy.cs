namespace RetrievalCore.QueryExecutor.QueryHandlers
{
	public class QueryHandlerDummy : IQueryHandler
	{
		public bool CanHandle(string subQuery)
		{
			return false;
		}
		
		public QueryResult Handle(QueryResult left, QueryResult right, string query)
		{
			return null;
		}
		
		public int Priority 
		{
			get 
			{
				return -1;
			}
		}
	}
}

