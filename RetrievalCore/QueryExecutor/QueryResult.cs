
using System.Collections.Generic;
namespace RetrievalCore.QueryExecutor
{
	public sealed class QueryResult
	{
		public QueryResult(int[] fileIds,
            IDictionary<int, IList<int>> positions)
		{
			FileIds = fileIds;
            Positions = positions;
		}

		public int[] FileIds { get; private set; }

        public IDictionary<int, IList<int>> Positions { get; private set; }
	}
}
