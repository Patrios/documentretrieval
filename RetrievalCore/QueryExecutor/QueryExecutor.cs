using System.Collections.Generic;
using System.Linq;
using RetrievalCore.QueryExecutor.QueryHandlers;

namespace RetrievalCore.QueryExecutor
{
	public sealed class QueryExecutor
	{
		public QueryExecutor (
			string defaultHandlerKey,
			TokenHandler tokenHandler,
			params IQueryHandler[] handlers)
		{
			_defaultHandlerKey = defaultHandlerKey;
			_tokenHandler = tokenHandler;
			_handlers = handlers;
		}

		public QueryResult Execute(string query)
		{
			_operators = new Stack<QueryResult> ();
			_functions = new Stack<IQueryHandler> ();
			_operatorKeywords = new Stack<string> ();

            query = Consts.OpenScope + query + Consts.CloseScope;

			foreach(var queryToken in ParseQuery(query))
			{
                if (queryToken.Equals(Consts.OpenScope))
				{
					_functions.Push(_openScopeHandler);
					continue;
				}

                if (queryToken.Equals(Consts.CloseScope))
				{
					while(_functions.Count > 0 && _functions.Peek() != _openScopeHandler)
					{
						PopFunction();
					}

					_functions.Pop(); // pop open scope handler
					continue;
				}

				var handler = FindHandler(queryToken);
				if(handler == null)
				{
					_operators.Push(_tokenHandler.Handle(queryToken));
				}
				else
				{
					while(CanPopFunction(handler))
					{
						PopFunction();
					}

					_functions.Push(handler);
					_operatorKeywords.Push(queryToken);
				}
			}

			return _operators.Peek ();
		}

        private bool IsToken(string token)
        {
            return FindHandler(token) == null && !token.Equals(Consts.OpenScope) && !token.Equals(Consts.CloseScope);
        }

		private bool CanPopFunction(IQueryHandler function)
		{
			if (_functions.Count == 0)
				return false;
			var p1 = function.Priority;
			var p2 = _functions.Peek().Priority;
			
			return p1 >= 0 && p2 >= 0 && p1 >= p2;
		}

		private void PopFunction()
		{
			var right = _operators.Pop ();
			var left = _operators.Pop ();

			_operators.Push(_functions.Pop().Handle(left, right, _operatorKeywords.Pop ()));
		}

		private IEnumerable<string> ParseQuery(string query)
		{
			var prevToken = string.Empty;
			foreach (var token in TokenParseTools.GetScopeSingleTokens(query)) 
			{
				if(prevToken != string.Empty)
				{
					if(IsNeedDefaultFunction(prevToken, token))
					{
						yield return _defaultHandlerKey;
					}
				}

                prevToken = token;
				yield return token;
			}
		}


		private bool IsNeedDefaultFunction(string operand1, string operand2)
		{
            return (IsToken(operand1) && IsToken(operand2))
                || (operand1.Equals(Consts.CloseScope) && operand2.Equals(Consts.OpenScope))
                || (IsToken(operand1) && operand2.Equals(Consts.OpenScope))
                || (operand1.Equals(Consts.CloseScope) && IsToken(operand2));
		}

		private IQueryHandler FindHandler(string token)
		{
			return _handlers.FirstOrDefault (x => x.CanHandle (token));
		}

		private Stack<string> _operatorKeywords;
		private Stack<QueryResult> _operators;
		private Stack<IQueryHandler> _functions;

		private readonly string _defaultHandlerKey;
		private readonly IQueryHandler[] _handlers;
		private readonly IQueryHandler _openScopeHandler = new QueryHandlerDummy();
		private readonly TokenHandler _tokenHandler; 
	}
}

