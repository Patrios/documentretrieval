﻿namespace RetrievalCore.TextSpliter
{
    public interface ITextSpliter
    {
        string[] Split(string text);
    }
}
