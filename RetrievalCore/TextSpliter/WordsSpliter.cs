﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RetrievalCore.TextSpliter
{
    public class WordsSpliter : ITextSpliter
    {
        public string[] Split(string text)
        {
            return ParseWords(text).ToArray();
        }

        private IEnumerable<string> ParseWords(string text)
		{
			var processed = new StringBuilder();
            
            text += ".";
            foreach (var ch in text)
			{
				if(char.IsLetter(ch))
				{
					processed.Append(ch);
				}
				else
				{
					var processWord = processed.ToString().Trim();
					if(processWord.Length != 0)
					{
						yield return processWord.ToLower ();
					}

					processed.Clear();
				}
			}
		}
    }
}
