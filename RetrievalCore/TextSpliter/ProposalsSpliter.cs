﻿namespace RetrievalCore.TextSpliter
{
    public class ProposalsSpliter : ITextSpliter
    {
        public string[] Split(string text)
        {
            return text.Split(Separators);
        }

        private static readonly char[] Separators = { '.', '?', '!' };
    }
}
