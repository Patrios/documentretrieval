﻿using System.Globalization;
using System.Linq;
using RetrievalCore.BinaryQueryParsers;
using RetrievalCore.TermsGarbegers.TokensBreaker;

namespace RetrievalCore.QueryParsers
{
    public sealed class QueryParser : IQueryParser
    {
        public QueryParser(IWordBreaker wordBreaker)
        {
            _wordBreaker = wordBreaker;
            ResultCount = Consts.RankinAll;
        }

        public string Parse(string query)
        {
            var words = _wordBreaker.BreakByWords(query).ToList();


            if (words.Last().StartsWith(NumberPrefix))
            {
                var count = int.Parse(words.Last().Replace(NumberPrefix, ""));
                words.RemoveAt(words.Count - 1);

                ResultCount = count;
            }

            return string.Join(Consts.Separator.ToString(CultureInfo.InvariantCulture), words.ToArray());
        }

        public int ResultCount { get; private set;  }

        private readonly IWordBreaker _wordBreaker;

        private const string NumberPrefix = "c";
    }
}
