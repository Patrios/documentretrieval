﻿namespace RetrievalCore.BinaryQueryParsers
{
    public interface IQueryParser
    {
        string Parse(string query);

        int ResultCount { get; }
    }
}
