using RetrievalCore.QueryExecutor;

namespace RetrievalCore.Ranking
{
	public interface IRanking
	{
		QueryResult Ranking(QueryResult result, int count); 
	}
}

