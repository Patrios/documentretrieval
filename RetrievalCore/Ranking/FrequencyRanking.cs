﻿using System.Collections.Generic;
using System.Linq;
using RetrievalCore.QueryExecutor;

namespace RetrievalCore.Ranking
{
    public sealed class FrequencyRanking : IRanking
    {
        public QueryResult Ranking(QueryResult result, int count)
        {
            var resultCount = count == Consts.RankinAll ? result.FileIds.Length : count;

            var iterms = result.Positions.OrderBy(x => x.Value.Count).Reverse().Take(resultCount);
            var ids = iterms.Select(x => x.Key).ToArray();

            var positions = new Dictionary<int, IList<int>>();

            foreach (var id in ids)
            {
                positions[id] = result.Positions[id];
            }

            return new QueryResult(ids, positions);
        }
    }
}
