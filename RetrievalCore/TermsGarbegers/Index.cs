using System;
using System.Collections.Generic;
using RetrievalCore.TermIndex;

namespace RetrievalCore.TermsGarbegers
{
    [Serializable]
	public class Index
	{
		public Index(IDictionary<string, ITermIndex> terms)
		{
			Terms = terms;
		}

        public IDictionary<string, ITermIndex> Terms { get; private set; }
	}
}