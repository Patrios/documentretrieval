using System.Collections.Generic;

namespace RetrievalCore.TermsGarbegers
{
	public interface IIndexBuilder
	{
		Index Build(IEnumerable<InMemoryFile> files);
	}
}

