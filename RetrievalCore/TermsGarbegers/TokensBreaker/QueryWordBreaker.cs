﻿using System.Collections.Generic;

namespace RetrievalCore.TermsGarbegers.TokensBreaker
{
    public sealed class QueryWordBreakerAdapter : IWordBreaker
    {
        public QueryWordBreakerAdapter(IWordBreaker wordBreaker)
        {
            _wordBreaker = wordBreaker;
        }

        public IEnumerable<string> BreakByWords(string content)
        {
// ReSharper disable LoopCanBeConvertedToQuery
            foreach (var word in _wordBreaker.BreakByWords(content))
// ReSharper restore LoopCanBeConvertedToQuery
            {
                if (word != Consts.WordKey)
                {
                    foreach (var scopeSingleWord in TokenParseTools.GetScopeSingleTokens(word))
                    {
                        yield return scopeSingleWord;
                    }
                }
            }
        }

        public int LastWordPosition
        {
            get { return _wordBreaker.LastWordPosition; }
        }

        private readonly IWordBreaker _wordBreaker;
    }
}
