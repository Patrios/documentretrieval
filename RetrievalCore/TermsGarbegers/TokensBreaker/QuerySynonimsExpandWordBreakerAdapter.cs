﻿using System.Collections.Generic;
using System.Linq;
using RetrievalCore.QueryExecutor.QueryHandlers;
using RetrievalCore.SynonimsManager;

namespace RetrievalCore.TermsGarbegers.TokensBreaker
{
    public sealed class QuerySynonimsExpandWordBreakerAdapter : IWordBreaker
    {
        public QuerySynonimsExpandWordBreakerAdapter(
            IWordBreaker wordBreaker, 
            ISynonimsManager synonimsManager)
        {
            _wordBreaker = wordBreaker;
            _synonimsManager = synonimsManager;
        }

        public IEnumerable<string> BreakByWords(string content)
        {
            foreach (var word in _wordBreaker.BreakByWords(content))
            {
                var synonims = _synonimsManager.GetSynonims(word).ToArray();
                if (synonims.Length > 0)
                {
                    yield return Consts.OpenScope;

                    foreach (var synonim in synonims)
                    {
                        yield return synonim;
                        yield return OrQueryHandler.OrPatternKeyword;
                    }

                    yield return word;
                    yield return Consts.CloseScope;
                }
                else
                {
                    yield return word;
                }
            }
        }

        public int LastWordPosition 
        {
            get { return _wordBreaker.LastWordPosition; }
        }

        private readonly IWordBreaker _wordBreaker;
        private readonly ISynonimsManager _synonimsManager;
    }
}
