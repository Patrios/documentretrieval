﻿using System.Collections.Generic;

namespace RetrievalCore.TermsGarbegers.TokensBreaker
{
    public interface IWordBreaker
    {
        IEnumerable<string> BreakByWords(string content);

        int LastWordPosition { get; }
    }
}
