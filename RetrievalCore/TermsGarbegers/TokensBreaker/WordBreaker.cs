﻿using System.Collections.Generic;
using System.Text;

namespace RetrievalCore.TermsGarbegers.TokensBreaker
{
    public sealed class WordBreaker : IWordBreaker
    {
        public WordBreaker()
        {
            LastWordPosition = 0;
        }

        public IEnumerable<string> BreakByWords(string content)
        {
            var processed = new StringBuilder();
            string processWord;
            var processPosition = 0;

            foreach (var ch in content)
            {
                if (char.IsLetterOrDigit(ch) || Consts.IsOperationChar(ch))
                {
                    if (processed.Length == 0)
                    {
                        LastWordPosition = processPosition;
                        yield return Consts.WordKey;
                    }

                    processed.Append(ch);
                }
                else
                {
                    processWord = processed.ToString().Trim();
                    if (processWord.Length != 0)
                    {
                        yield return processWord.ToLower();
                    }

                    processed.Clear();
                }

                processPosition++;
            }

            processWord = processed.ToString().Trim();
            if (processWord.Length != 0)
            {
                yield return processWord.ToLower();
            }
        }

        public int LastWordPosition { get; private set; }

    }
}
