﻿using System.Collections.Generic;
using Iveonik.Stemmers;

namespace RetrievalCore.TermsGarbegers.TokensBreaker
{
    public sealed class StremmingWordBreakerAdapter : IWordBreaker
    {
        public StremmingWordBreakerAdapter(IWordBreaker wordBreaker, IStemmer stemmer)
        {
            _wordBreaker = wordBreaker;
            _stemmer = stemmer;
        }

        public IEnumerable<string> BreakByWords(string content)
        {
            foreach (var word in _wordBreaker.BreakByWords(content))
            {
                if (Consts.IsOperationWord(word))
                {
                    yield return word;
                }
                else
                {
                    yield return _stemmer.Stem(word);
                }
            }
        }

        public int LastWordPosition 
        {
            get { return _wordBreaker.LastWordPosition; }
        }

        private readonly IWordBreaker _wordBreaker;
        private readonly IStemmer _stemmer;
    }
}
