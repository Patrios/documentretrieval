using System.Collections.Generic;
using RetrievalCore.TermIndex;
using RetrievalCore.TermsGarbegers.TokensBreaker;

namespace RetrievalCore.TermsGarbegers
{
	public class IndexBuilder : IIndexBuilder
    {
        public IndexBuilder(IWordBreaker wordBreaker)
        {
            _wordBreaker = wordBreaker;
        }

		public Index Build(IEnumerable<InMemoryFile> files)
		{
			var terms = new Dictionary<string, ITermIndex> ();

			foreach (var file in files)
			{
				ProcessFile(file, terms);
			}

			return new Index(terms);
		}

        private void ProcessFile(InMemoryFile file, IDictionary<string, ITermIndex> terms)
		{
			foreach (var token in _wordBreaker.BreakByWords(file.Content)) 
			{
				if(terms.ContainsKey(token))
				{
                    terms[token].AddNewPosition(file.Id, _wordBreaker.LastWordPosition);
				}
				else 
				{
                    var newPositions = new Dictionary<int, IList<int>>
                    {
                        {
                            file.Id, new List<int> { _wordBreaker.LastWordPosition }
                        }
                    };

				    terms.Add(
                        new KeyValuePair<string, ITermIndex>(
                            token,
                            new UncompressedTermIndex(newPositions)));
				}
			}
		}

	    private readonly IWordBreaker _wordBreaker;
	}
}

