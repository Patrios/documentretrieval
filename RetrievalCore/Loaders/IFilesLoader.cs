using System.Collections.Generic;
using System;

namespace RetrievalCore.Loaders
{
	public interface IFilesLoader
	{
		IList<InMemoryFile> LoadFiles(string localPath);
	}
}

