﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace RetrievalCore.Loaders
{
    public class WarAndPeaceLoader : IFilesLoader
    {
        public IList<InMemoryFile> LoadFiles(string localPath)
        {
            var stringBuilder = new StringBuilder();
            using (var bookFileReader = new StreamReader(localPath, Encoding.GetEncoding(TextSourceEncoding)))
            {
                var encoding = bookFileReader.CurrentEncoding;

                while (!bookFileReader.EndOfStream)
                {
                    var fileLine = bookFileReader.ReadLine();
                    var utfLine = ConvertToUtf8(fileLine, encoding);

                    if (IsBookText(utfLine))
                    {
                        var cleanedLine = CleanOfTags(CleanForStartTresh(utfLine));
                        stringBuilder.Append(" ");
                        stringBuilder.Append(cleanedLine);
                    }
                }
            }

            return new InMemoryFile[] { new InMemoryFile(stringBuilder.ToString()) };
        }

        private string CleanOfTags(string processLine)
        {
            return Regex.Replace(processLine, StartTagRegexp, String.Empty);
        }

        private bool IsBookText(string processLine)
        {
            return processLine.StartsWith(TextStartTag);
        }

        private string ConvertToUtf8(string text, Encoding encoding)
        {
            var encBytes = encoding.GetBytes(text);
            var utf8Bytes = Encoding.Convert(encoding, Encoding.UTF8, encBytes);
            var utf8Content = Encoding.UTF8.GetString(utf8Bytes);

            return utf8Content;
        }

        private string CleanForStartTresh(string processLine)
        {
            return processLine.Replace(TextStartTag, String.Empty);
        }

        private const string TextStartTag = "<dd>&nbsp;&nbsp;";
        private const string TextSourceEncoding = "windows-1251";
        private const string StartTagRegexp = @"<[^>]*>";
    }
}
