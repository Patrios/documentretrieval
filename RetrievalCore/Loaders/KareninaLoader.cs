using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RetrievalCore.Loaders
{
	public class KareninaLoader : IFilesLoader
	{
	    public IList<InMemoryFile> LoadFiles(string localPath)
		{
			var files = new List<InMemoryFile> ();

			using (var bookFileReader = new StreamReader(localPath, Encoding.GetEncoding(TextSourceEncoding))) 
			{
				var encoding = bookFileReader.CurrentEncoding;

				while(!bookFileReader.EndOfStream)
				{
					var firstLine = SkipToFileBegin(bookFileReader);
					
					if(!bookFileReader.EndOfStream)
					{
						var fileContent = CollectText(bookFileReader, firstLine);

                        // ReSharper disable PossibleUnintendedReferenceComparison
						if(encoding != Encoding.UTF8)
                        // ReSharper restore PossibleUnintendedReferenceComparison
						{
							fileContent = ConvertToUtf8(fileContent, encoding);
						}

						files.Add(new InMemoryFile(fileContent));
					}
				}
			}

			return files;
		}

		private string ConvertToUtf8(string text, Encoding encoding)
		{
			var encBytes = encoding.GetBytes(text);
			var utf8Bytes = Encoding.Convert(encoding, Encoding.UTF8, encBytes);
			var utf8Content = Encoding.UTF8.GetString(utf8Bytes);

			return utf8Content;
		}

		private string CollectText(StreamReader bookFileReader, string firstLine)
		{
			var result = new StringBuilder ();
			var fileLine = firstLine;
			while (!fileLine.StartsWith(TextEndTag) && !bookFileReader.EndOfStream) 
			{
				result.Append(fileLine.Replace(StartTresh, ""));
				fileLine = bookFileReader.ReadLine ();
                // ReSharper disable PossibleNullReferenceException
				if(fileLine.Contains(EndText))
                // ReSharper restore PossibleNullReferenceException
				{
					bookFileReader.ReadToEnd();
				}
			}

			return result.ToString ();
		}

		private string SkipToFileBegin(StreamReader bookFileReader)
		{
			var fileLine = "";

            // ReSharper disable PossibleNullReferenceException
			while (!fileLine.StartsWith(TextStartTag) && !bookFileReader.EndOfStream) 
            // ReSharper restore PossibleNullReferenceException
			{
				fileLine = bookFileReader.ReadLine();
			}

			return fileLine;
		}

		private const string TextSourceEncoding = "windows-1251";
		private const string TextStartTag = "<dd>";
		private const string TextEndTag = "<div";
		private const string StartTresh = "<dd>&nbsp;&nbsp; ";
		private const string EndText = "<!--Section Ends-->";
	}
}

