﻿using System.Linq;

namespace RetrievalCore
{
    public static class Consts
    {
        public const string WordKey = "WORD";
        public const string OpenScope = "(";
        public const string CloseScope = ")";

        public const int SnippetSize = 1;

        public const double StudentTestLimit = 2.576;
        public const double PirsonTestLimit = 3.841;

        public const char Separator = ' ';
        public const int RankinAll = -1;

        public static bool IsOperationWord(string word)
        {
            return word == WordKey || word == OpenScope || word == CloseScope;
        }
        public static bool IsOperationChar(char ch)
        {
            return ch == WordKey.First() || ch == OpenScope.First() || ch == CloseScope.First();
        }

        public const string LocalSerializedTerms = "AnalyzedBook";

        public const string LocalKareninaName = "Karenina";

        public const string KareninaPath = @"http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0080.shtml";
    }
}
