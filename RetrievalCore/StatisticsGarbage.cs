using System.Linq;
using RetrievalCore.TermsGarbegers;

namespace RetrievalCore
{
	public class StatisticsGarbage
	{
		public StatisticsGarbage(Index statistics)
		{
			_statistics = statistics;
		}

		public float AvgTermLength()
		{
			var totalLength = _statistics.Terms.Sum(valueKey => valueKey.Key.Length);

		    return totalLength / (float)_statistics.Terms.Count;
		}
		
		public float AvgTokenLength()
		{
			var totalLength = 0;
			var totalCount = 0;
			foreach (var valueKey in _statistics.Terms) 
			{
				totalLength += valueKey.Key.Length * valueKey.Value.TotalCount;
				totalCount += valueKey.Value.TotalCount;
			}
			
			return totalLength / (float)totalCount;
		}

		private readonly Index _statistics; 
	}
}

