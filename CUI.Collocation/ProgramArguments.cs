﻿namespace CUI.Collocation
{
    public class ProgramArguments
    {
        public ProgramArguments(string[] args)
        {
            if (args.Length == 2 && args[0] == WindowKey)
            {
                var passedValue = int.Parse(args[1]);

                if (passedValue < MinWindowsSize)
                {
                    WindowSize = MinWindowsSize;
                }
                else if (passedValue > MaxWindowsSize)
                {
                    WindowSize = MaxWindowsSize;
                }
                else
                {
                    WindowSize = passedValue;
                }
            }
            else
            {
                WindowSize = MinWindowsSize;
            }
        }

        public int WindowSize { get; private set; }

        private const string WindowKey = "-w";
        private const int MinWindowsSize = 2;
        private const int MaxWindowsSize = 5;
    }
}
