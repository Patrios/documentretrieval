﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RetrievalCore;
using RetrievalCore.CollocationGarbager;
using RetrievalCore.Loaders;
using RetrievalCore.TextSpliter;

namespace CUI.Collocation
{
    // ReSharper disable RedundantNameQualifier
    // ReSharper disable SpecifyACultureInStringConversionExplicitly
    // ReSharper disable ClassNeverInstantiated.Global
    class CollocationProgram
    // ReSharper restore ClassNeverInstantiated.Global
    {
        static void Main(string[] args)
        {
            var arguments = new ProgramArguments(args);
            
            CreateBigramGarbager(arguments.WindowSize);

            DownloadBooks();
            var books = ParceBooks();
            var wholeContent = CollectContent(books);
            var bigrams = CollectBigrams(wholeContent);

            CalculateStatstic(bigrams);
            var sortedByFrequencyBigrams = bigrams
                .BigramsStatistics
                .OrderByDescending(x => x.Value.Count)
                .ToArray();

            var studentSortedBigrams = bigrams
                .BigramsStatistics
                .OrderByDescending(x => x.Value.StudentTest)
                .Where(x => x.Value.StudentTest > Consts.StudentTestLimit)
                .ToArray();

            var pirsonSortedBigrams = bigrams
                .BigramsStatistics
                .OrderByDescending(x => x.Value.PirsonTest)
                .Where(x => x.Value.PirsonTest > Consts.PirsonTestLimit)
                .ToArray();

            var filtredStudentSortedBigrams = bigrams
                .BigramsStatistics
                .OrderByDescending(x => x.Value.StudentTest)
                .Where(x => x.Value.StudentTest <= Consts.StudentTestLimit)
                .ToArray();

            var filtredPirsonSortedBigrams = bigrams
                .BigramsStatistics
                .OrderByDescending(x => x.Value.PirsonTest)
                .Where(x => x.Value.PirsonTest <= Consts.PirsonTestLimit);

            WriteBigramsToCsvFile(sortedByFrequencyBigrams, FrequenceSortedBigramFile);
            WriteBigramsToCsvFile(studentSortedBigrams, StudentSortedBigramFile);
            WriteBigramsToCsvFile(pirsonSortedBigrams, PirsonSortedBigramFile);
            WriteBigramsToCsvFile(filtredStudentSortedBigrams, FiltredStudentSortedBigramFile);
            WriteBigramsToCsvFile(filtredPirsonSortedBigrams, FiltredPirsonSortedBigramFile);
            // WriteBigramsToCsvFile(likelihoodSortedBigrams, LikelihoodSortedBigramFile);

            System.Console.WriteLine(
                "Most used bigram: \"{0} {1}\" pirson test {2}",
                sortedByFrequencyBigrams[0].Key.LeftWord,
                sortedByFrequencyBigrams[0].Key.RightWord,
                Math.Round(sortedByFrequencyBigrams[0].Value.PirsonTest, 3));

            System.Console.WriteLine("Bigrams count: " + bigrams.BigramsCount);

            System.Console.WriteLine("For exit type any key.");
            System.Console.ReadKey();
        }

        private static void CalculateStatstic(BigramGarbageResult bigrams)
        {
            System.Console.WriteLine("Calculate statistics.");
            bigrams.CalculateStatstic();
            System.Console.WriteLine("Calculate fininshed.\n");
        }

        private static void WriteBigramsToCsvFile(IEnumerable<KeyValuePair<Bigram, BigramStatistics>> bigrams, string fileName)
        {
            System.Console.WriteLine("Save bigrams to " + fileName);

            using (var streamWriter = new StreamWriter(fileName))
            {
                streamWriter.WriteLine(
                    "{0},{1},{2},{3},{4}",
                    Quots("Левое слово"),
                    Quots("Правое слово"),
                    Quots("Количество вхождений"),
                    Quots("Критерий Стюдента"),
                    Quots("Критерий Пирса"));
                    // Quots("Критерий отношения правдоподобия"));

                const int roundDigits = 3;
                foreach (var bigram in bigrams)
                {
                    streamWriter.WriteLine(
                        "{0},{1},{2},{3},{4}",
                        Quots(bigram.Key.LeftWord),
                        Quots(bigram.Key.RightWord),
                        Quots(bigram.Value.Count.ToString()),
                        Quots(Math.Round(bigram.Value.StudentTest, roundDigits).ToString()),
                        Quots(Math.Round(bigram.Value.PirsonTest, roundDigits).ToString()));
                    // Quots(Math.Round(bigram.Value.LikelihoodTest, roundDigits).ToString()));
                }
            }

            System.Console.WriteLine("Save completed.\n");
        }

        private static string Quots(string text)
        {
            return "\"" + text + "\"";
        }

        private static BigramGarbageResult CollectBigrams(string text)
        {
            System.Console.WriteLine("Collect bigrams.");
            var result = _bigramGarbager.Collect(text);
            System.Console.WriteLine("Collection completed.\n");

            return result;
        }

        private static string CollectContent(IEnumerable<InMemoryFile> files)
        {
            var stringBuilder = new StringBuilder();

            foreach (var file in files)
            {
                stringBuilder.Append(file.Content);
            }

            return stringBuilder.ToString();
        }

        private static void DownloadBooks()
        {
            if (!Directory.Exists(CorpusPath))
            {
                Directory.CreateDirectory(CorpusPath);
            }

            System.Console.WriteLine("Download books started.");
            Downloader.DownloadKarenina(CorpusPath);
            Downloader.DownloadWarAndPeace1(CorpusPath);
            Downloader.DownloadWarAndPeace2(CorpusPath);
            Downloader.DownloadWarAndPeace3(CorpusPath);
            Downloader.DownloadWarAndPeace4(CorpusPath);
            System.Console.WriteLine("Download finished.\n");
        }

        private static IEnumerable<InMemoryFile> ParceBooks()
        {
            System.Console.WriteLine("Parse books.");
            var files = new List<InMemoryFile>();
            files.AddRange(WarAndPeaceLoader.LoadFiles(Path.Combine(CorpusPath, Downloader.WarAndPeace1Path)));
            files.AddRange(WarAndPeaceLoader.LoadFiles(Path.Combine(CorpusPath, Downloader.WarAndPeace2Path)));
            files.AddRange(WarAndPeaceLoader.LoadFiles(Path.Combine(CorpusPath, Downloader.WarAndPeace3Path)));
            files.AddRange(WarAndPeaceLoader.LoadFiles(Path.Combine(CorpusPath, Downloader.WarAndPeace4Path)));
            files.AddRange(WarAndPeaceLoader.LoadFiles(Path.Combine(CorpusPath, Downloader.KareninaLocalPath)));
            System.Console.WriteLine("Parse finished.\n");

            return files;
        }

        private static void CreateBigramGarbager(int windowSize)
        {
            _bigramGarbager = new BigramGarbager(
                new ProposalsSpliter(), 
                new WordsSpliter(),
                windowSize);
        }

        private const string CorpusPath = "corpus";
        private static readonly BookDownloader Downloader = new BookDownloader();
        private static readonly IFilesLoader WarAndPeaceLoader = new WarAndPeaceLoader();

        private static IBigramGarbager _bigramGarbager;

        private const string FrequenceSortedBigramFile = "FrequenceBigrams.csv";
        private const string StudentSortedBigramFile = "StudentBigrams.csv";
        private const string PirsonSortedBigramFile = "PirsonBigrams.csv";
        private const string FiltredStudentSortedBigramFile = "FiltredStudentBigrams.csv";
        private const string FiltredPirsonSortedBigramFile = "FiltredPirsonBigrams.csv";
    }
}