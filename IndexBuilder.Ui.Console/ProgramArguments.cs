﻿namespace IndexBuilder.Ui.Console
{
    public class ProgramArguments
    {
        public ProgramArguments(string[] args)
        {
            if (args.Length == 1 && args[0] == VariableByteKey)
            {
                IsVariableByte = true;
            }
        }

        public bool IsVariableByte { get; private set; }

        private const string VariableByteKey = "-vb";
    }
}
