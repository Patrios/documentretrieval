﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using RetrievalCore;
using RetrievalCore.Compressors;
using RetrievalCore.Loaders;
using RetrievalCore.TermsGarbegers.TokensBreaker;

namespace IndexBuilder.Ui.Console
{
    public static class IndexBuilderProgram
    {
        static void Main(string[] args)
        {
            var programArgs = new ProgramArguments(args);

            var fileLoader = new KareninaLoader();

            if (!File.Exists(Consts.LocalKareninaName))
            {
                WebLoader.LoadPage(Consts.KareninaPath, Consts.LocalKareninaName);
            }

            var files = fileLoader.LoadFiles(Consts.LocalKareninaName);

            var fileAnalyzer = new RetrievalCore.TermsGarbegers.IndexBuilder(
                    new WordBreaker());

            System.Console.WriteLine("Start Construct Index...");

            var filesIndex = fileAnalyzer.Build(files);

            System.Console.WriteLine("Construct completed!");

            IDataCodec codec;

            if (programArgs.IsVariableByte)
            {
                codec = new VariableByteCodec();
            }
            else
            {
                codec = new EliasGammaCodec();
            }

            System.Console.WriteLine("Start Compression...");

            var compressedIndex = IndexCompressor.Compress(filesIndex, codec);

            System.Console.WriteLine("Compression Finished.");

            System.Console.WriteLine("Start Save Index to: " + Consts.LocalSerializedTerms);
            
            var formatter = new BinaryFormatter();
            var fs = new FileStream(Consts.LocalSerializedTerms, FileMode.OpenOrCreate);
            formatter.Serialize(fs, compressedIndex);

            System.Console.WriteLine("Save completed!");

            System.Console.ReadKey();
        }
    }
}
