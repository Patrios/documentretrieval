using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Iveonik.Stemmers;
using RetrievalCore;
using RetrievalCore.BinaryQueryParsers;
using RetrievalCore.Loaders;
using RetrievalCore.QueryExecutor;
using RetrievalCore.QueryExecutor.QueryHandlers;
using RetrievalCore.QueryParsers;
using RetrievalCore.Ranking;
using RetrievalCore.SnippetBuilder;
using RetrievalCore.SynonimsManager;
using RetrievalCore.TermsGarbegers;
using RetrievalCore.TermsGarbegers.TokensBreaker;

namespace BinarSearch.Ui.Console
{
	public class ConsoleContainer
	{
		public ConsoleContainer ()
        {
            Stemmer = new RussianStemmer();

			FileLoader = new KareninaLoader ();

            if (!File.Exists(Consts.LocalKareninaName))
            {
                WebLoader.LoadPage(Consts.KareninaPath, Consts.LocalKareninaName);
            }

            Files = FileLoader.LoadFiles(Consts.LocalKareninaName);

		    DeserializeTermsGarbageResult();

			Executor = new QueryExecutor (
				OrQueryHandler.OrPatternKeyword,
				new TokenHandler(AnalyzedFiles),
				new AndQueryHandler(),
				new OrQueryHandler());

			Ranking = new FrequencyRanking();

            SnippetBuilder = new FileAsSnippetBuilder(Files, AnalyzedFiles);

            QueryParser = new QueryParser(
                    new QueryWordBreakerAdapter(new WordBreaker()));

            StatisticsGarbage = new StatisticsGarbage(AnalyzedFiles);
		}

        public IQueryParser QueryParser { get; private set; }

        public QueryExecutor Executor { get; private set; }

        public IRanking Ranking { get; private set; }

        public ISnippetBuilder SnippetBuilder { get; private set; }

	    public StatisticsGarbage StatisticsGarbage { get; private set; }

	    private void CreateNewSynonimsManager()
	    {
	        var synonimsParser = new SynonimsParser(new QueryWordBreakerAdapter(new WordBreaker()));

	        var synonims = synonimsParser.Parse(SynonimsFile);
            SynonimsManager = new FileBasedSynionimsManager(synonims);
	    }

	    private void DeserializeSynonimsManager()
	    {
            var formatter = new BinaryFormatter();
            using (var fileStream = new FileStream(SynonimsParsedFile, FileMode.Open))
            {
                var synonims = (IDictionary<string, List<string>>)formatter.Deserialize(fileStream);
                SynonimsManager = new FileBasedSynionimsManager(synonims);
            }
	    }

	    private void DeserializeTermsGarbageResult()
	    {
            var formatter = new BinaryFormatter();
            var fs = new FileStream(Consts.LocalSerializedTerms, FileMode.Open);
            AnalyzedFiles = (Index)formatter.Deserialize(fs);
	    }
        private ISynonimsManager SynonimsManager { get; set; }

	    private IStemmer Stemmer { get; set; }

	    private IFilesLoader FileLoader { get; set; }

	    private IList<InMemoryFile> Files { get; set; }

	    private Index AnalyzedFiles { get; set; }

        private const string SynonimsParsedFile = "SynonimsParsed";

        private const string SynonimsFile = "synonims.txt";
	}
}

