namespace BinarSearch.Ui.Console
{
	public sealed class MainClass
	{
		public static void Main ()
		{

            System.Console.WriteLine("Preparation...");
			
			_container = new ConsoleContainer ();

            System.Console.WriteLine("Preparation completed!\n");

            System.Console.WriteLine("Statistics:");
            System.Console.WriteLine("Avg Term Length: {0}", _container.StatisticsGarbage.AvgTermLength());
            System.Console.WriteLine("Avg Token Length: {0}\n", _container.StatisticsGarbage.AvgTokenLength());

		    while (true) 
			{
				System.Console.Write("> ");
// ReSharper disable PossibleNullReferenceException
			    var userQuery = System.Console.ReadLine();
// ReSharper restore PossibleNullReferenceException
				if(userQuery == QuitText)
				{
					break;
				}

                var parsedQuery = _container.QueryParser.Parse(userQuery);
                if (string.IsNullOrEmpty(parsedQuery))
				{
					continue;
				}

                var queryResult = _container.Executor.Execute(parsedQuery);
                var rankedResult = _container.Ranking.Ranking(queryResult, _container.QueryParser.ResultCount);

				if(rankedResult.FileIds.Length == 0)
				{
					System.Console.WriteLine("There is no results.");
				}
				else
				{
					var resultNumber = 0;
					System.Console.WriteLine("Results:");
                    foreach (var snippet in _container.SnippetBuilder.CollectResults(rankedResult))
					{
						System.Console.WriteLine("File number: " + rankedResult.FileIds[resultNumber]);
						System.Console.WriteLine(snippet + "\n\n");
						resultNumber++;
					}
				}
			}
		}

		private const string QuitText = "quit";
		private static ConsoleContainer _container;
	}
}
